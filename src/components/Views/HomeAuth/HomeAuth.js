import React, { useState } from 'react';
import { useNavigate } from 'react-router';
import Login from '../../UI/Login/Login.js'

export default function HomeAuth() {

    const history = useNavigate();
    const [isAuthenticated, setIsAuthenticated] = useState(false);
    const token = localStorage.getItem('token');

    React.useEffect(() => {
        if (token) {
            setIsAuthenticated(true);
        } else {
            history.push('/');
        }
    }, [token, history]);

    const handleLogout = () => {
        localStorage.removeItem('token');
        setIsAuthenticated(false);
        history.push('/');
    };

    if (!isAuthenticated) {
        return <Login onLogin={() => setIsAuthenticated(true)} />;
    }

    return (
        <>
            <h1>Welcome to the Private Section</h1>
            <button onClick={handleLogout}>Logout</button>
        </>
    );
}