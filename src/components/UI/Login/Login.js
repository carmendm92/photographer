import React, { useState } from 'react';
import axios from 'axios';

export default function Login({ onLogin }) {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const api = axios.create({
        baseURL: 'http://localhost:3000',
    });
    const handleSubmit = async (event) => {
        event.preventDefault();
        try {
            // chiamata API per l'autenticazione
            const response = await api.post('/', { email, password });
            // salva il token nello storage del browser
            localStorage.setItem('token', response.data.token);
            onLogin();
        } catch (error) {
            console.error(error);
        }
    }

    return (
        <>
            <div className="container">
                <div className="row">

                    <div className="col-12 text-center mt-5">
                        <h1 className="fw-bolder fa-3x">Login</h1>
                    </div>
                </div>

                <div className="row justify-content-center align-items-center mt-5">
                    <div className="col-12 col-md-4">
                        <form onSubmit={handleSubmit}>

                            <div className="mb-3">
                                <label>Email</label>
                                <input
                                    className="form-control"
                                    type="email"
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                />
                            </div>

                            <div className="mb-3">
                                <label> Password</label>
                                <input
                                    className="form-control"
                                    type="password"
                                    value={password}
                                    onChange={(e) => setPassword(e.target.value)}
                                />
                            </div>

                            <div className="text-center">
                                <button className="btn btn-primary mb-4" type="submit">Login</button>
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </>
    );
}



