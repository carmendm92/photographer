import { useAuth } from './useAuth';

function LogoutButton() {

    const { logout } = useAuth();

    return (
        <button onClick={logout}>
            Logout
        </button>
    );
}

export default LogoutButton; 