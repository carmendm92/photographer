import Home from './components/Views/Home/Home.js'
import HomeAuth from './components/Views/HomeAuth/HomeAuth.js';
import { Routes, Route } from 'react-router-dom';
import './App.css';

export default function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/homeAuth" element={<HomeAuth />} />
      </Routes>
    </>

  );
}
